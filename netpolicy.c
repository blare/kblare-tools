/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include "lib.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]){
	int rc;
	const char *sysfs_path = "/sys/kernel/security/blare/network";
	FILE *f;
	struct tags_t tags;

	rc = get_tags_from_command(argc,argv,&tags);
	if (rc < 0)
		return rc;

	if (rc == 0){
		printf("No tags given...\n");
		return -2;
	}

	printf("Will write %d tags to %s\n",tags.size,sysfs_path);

	f = fopen(sysfs_path,"w");
	rc = fwrite(&tags.values,sizeof(int),tags.size,f);
	fputc (EOF,f);
	fclose(f);

	free_tags(&tags);

	return rc;
}

