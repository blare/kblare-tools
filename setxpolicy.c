/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include <sys/types.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <stdlib.h>
#include "lib.h"
#include <regex.h>


int main (int argc, char **argv){
	char *filename;
	int rc;
	struct policy_t policy;

	// argc should at least be 2 
	if (argc < 2){
		printf("Usage: setxpolicy [filename] {set1} {set2} ... {setn} \n");
		return -1;
	}

	filename = argv[1];

	rc = get_policy_from_command(argc, argv, &policy);
	if (rc <0) {
		printf("Could not parse execute policy tags properly.\n");
		return rc;
	}

	rc = write_policy(filename, policy, XPOLICY);
	if (rc <0) {
		printf("Could not write execute policy tags.\n");
		return rc;
	}

	free_policy(&policy);

	printf("Successfuly wrote execute policy tags\n");
	return rc;
}


