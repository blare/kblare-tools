#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <blare/policy.h>
#include "lib.h"

int main(int argc, char **argv)
{
	int i         = 0,
	    offset    = 0,
	    //nb_policy = 0,
	    size      = 0,
	    //position  = 0,
	    //*array    = NULL,
	    *buffer   = NULL,
	    *sizes    = NULL;
	struct policy_t policy;

	get_policy_from_command(argc, argv, &policy);
	sizes = (int*) malloc(policy.size * sizeof(int));
	for (i=0; i<policy.size; i++)
		size += policy.tags[i].size;
	buffer = (int*) malloc(size * sizeof(int));
	for (i=0; i<policy.size; i++) {
		sizes[i] = policy.tags[i].size;
		memcpy(&buffer[offset],
				policy.tags[i].values, sizes[i] * sizeof(int));
		offset += sizes[i];
	}


//	while ((position = identify_sets(position, argv, argc,
//					&array, &size)) > 0) {
//		if (buffer == NULL) {
//			buffer = (int *)malloc(size * sizeof(int));
//			sizes  = (int *)malloc(++nb_policy * sizeof(int));
//		} else {
//			buffer = (int *)realloc(buffer, (offset + size) * sizeof(int));
//			sizes  = (int *)realloc(sizes, ++nb_policy * sizeof(int));
//		}
//
//		sizes[nb_policy - 1] = size;
//		memcpy(buffer + offset, array, size * sizeof(int));
//		offset += size;
//
//		free(array);
//	}

	set_policy(policy.size, sizes, buffer);

	free_policy(&policy);
	return 0;
}
