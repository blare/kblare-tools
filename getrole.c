/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include <sys/types.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <stdlib.h>
#include "lib.h"
#include <regex.h>

/* Reads the policy attached to the given UID/role
 * User roles are located in /etc/blare in the extended attribures
 * of files named after the UID/role in /etc/blare
 * */
int main (int argc, char **argv){
	char *uid, *filename, *xname;
	const char *folder = "/etc/blare/";
	int pathsize, rc, xname_size, i, j;
	struct policy_t role;

	if (argc < 2){
		printf("Usage: getpolicy role_number/uid\n");
		return -1;
	}

	uid = argv[1];
	pathsize = sizeof(*uid) + sizeof(*folder);
	filename = (char*)malloc(pathsize);

	snprintf(filename,pathsize,"%s%s",folder,uid);

	rc = get_policy_from_file(filename, &role, ROLE);
	if(rc<0) {
		printf("Could not read role.\n");
		return -1;
	}

	xname_size = sizeof(ROLE_XATTR) + sizeof(int);
	xname = malloc(xname_size);

	printf("%s: %d\n",ROLE_COUNT, role.size);
	for (i=0; i<role.size; i++) {
		snprintf (xname, xname_size,"%s%d", ROLE_XATTR,i);
		printf("%s: ",xname);
		for(j=0;j<role.tags[i].size;j++)
			printf("%d ",role.tags[i].values[j]);
		printf("\n");
	}

	free_policy(&role);
	free(xname);
	free(filename);
	return rc;
}


