/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include "lib.h"
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
	int rc, xname_size, i;
	char *filename, *xname;
	struct tags_t xpolicy;

	if (argc < 2) {
		printf("Usage: delxpolicy filename\n");
		return -1;
	}

	filename = argv[1];

	// Get the XPOLICY_COUNT
	rc = get_tags_from_file(filename, XPOLICY_COUNT, &xpolicy);
	if(rc<0){
		printf("Could not get the xpolicy count : error %d\n",xpolicy.values[0]);
		return -1;
	}

	xname_size = sizeof(XPOLICY_XATTR) + sizeof(int);
	xname = (char*)malloc(xname_size);

	for(i=0;i<xpolicy.values[0];i++){
		snprintf (xname, xname_size,"%s%d", XPOLICY_XATTR,i);
		rc = removexattr(filename,xname);
		if (rc < 0)
			printf("Error %d\n", rc);
	}

	/* Remove the xpolicy count */
	rc = removexattr(filename,XPOLICY_COUNT);

	free(xname);
	free_tags(&xpolicy);
	printf("Deleted execution policy tags.\n");
	return rc;
}
