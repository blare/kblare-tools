/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include "lib.h"
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
	int rc, n, i;
	char *filename,*list;

	if (argc < 2)
		return -1;

	filename = argv[1];
	n = get_list_length(filename);
	if (n < 0) {
		printf("Error %d reading xattr fields...\n",n);
		return n;
	}
	list = (char*) malloc(n * sizeof(char));
	n = get_list(filename, list, n);
	if (n < 0) {
		printf("Error %d reading xattr fields...\n",n);
		return n;
	}

	i=0;
	while (i<n)
		i+=printf("%s\n", &list[i]);


	free(list);
	return rc;
}
