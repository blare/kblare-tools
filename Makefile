TARGETS = setinfo getinfo list_xattr setpolicy getpolicy delinfo delpolicy delxpolicy setxpolicy getxpolicy setrole getrole netpolicy trace
COOP_TARGETS = setcpolicy
RM = rm -f
CP = /bin/cp
DESTDIR = /usr/local/bin

all: $(TARGETS)

coop: $(TARGETS) $(COOP_TARGETS)

clean: 
		$(RM) *.o $(TARGETS) $(COOP_TARGETS)

install:
		$(CP) $(TARGETS) $(COOP_TARGETS) $(DESTDIR)

setcpolicy: lib.o setcpolicy.o
		gcc lib.o setcpolicy.o -o setcpolicy -lapi

setcpolicy.o: setcpolicy.c
		gcc -c setcpolicy.c

setinfo : lib.o setinfo.o
		gcc lib.o setinfo.o -o setinfo

delinfo : lib.o delinfo.o
		gcc lib.o delinfo.o -o delinfo

setpolicy : lib.o setpolicy.o
		gcc lib.o setpolicy.o -o setpolicy

setrole : lib.o setrole.o
		gcc lib.o setrole.o -o setrole

setxpolicy : lib.o setxpolicy.o
		gcc lib.o setxpolicy.o -o setxpolicy

getinfo : lib.o getinfo.o
		gcc lib.o getinfo.o -o getinfo

getpolicy : lib.o getpolicy.o
		gcc lib.o getpolicy.o -o getpolicy

getrole : lib.o getrole.o
		gcc lib.o getrole.o -o getrole

getxpolicy : lib.o getxpolicy.o
		gcc lib.o getxpolicy.o -o getxpolicy

delpolicy : lib.o delpolicy.o
		gcc lib.o delpolicy.o -o delpolicy

delxpolicy : lib.o delxpolicy.o
		gcc lib.o delxpolicy.o -o delxpolicy

list_xattr : lib.o list_xattr.o
		gcc lib.o list_xattr.o -o list_xattr

netpolicy : lib.o netpolicy.o
		gcc lib.o netpolicy.o -o netpolicy

setinfo.o : setinfo.c
		gcc -c setinfo.c 

delinfo.o : delinfo.c
		gcc -c delinfo.c

setpolicy.o : setpolicy.c
		gcc -c setpolicy.c 

setrole.o : setrole.c
		gcc -c setrole.c

setxpolicy.o : setxpolicy.c
		gcc -c setxpolicy.c

delpolicy.o : delpolicy.c
		gcc -c delpolicy.c

delxpolicy.o : delxpolicy.c
		gcc -c delxpolicy.c

getinfo.o : getinfo.c
		gcc -c getinfo.c

getpolicy.o : getpolicy.c
		gcc -c getpolicy.c

getrole.o : getrole.c
		gcc -c getrole.c

getxpolicy.o : getxpolicy.c
		gcc -c getxpolicy.c

list_xattr.o : list_xattr.c
		gcc -c list_xattr.c

lib.o : lib.c
		gcc -g -c lib.c

netpolicy.o: netpolicy.c
		gcc -c netpolicy.c

trace: trace.o
		gcc trace.o -o trace
trace.o: trace.c
		gcc -c trace.c

