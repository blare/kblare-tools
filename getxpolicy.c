/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include <sys/types.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <stdlib.h>
#include "lib.h"
#include <regex.h>

int main (int argc, char **argv){
	char *filename, *xname;
	int rc, xname_size, i, j;
	struct policy_t xpolicy;

	if (argc < 2){
		printf("Usage: getpolicy filename\n");
		return -1;
	}

	filename = argv[1];
	rc = get_policy_from_file(filename, &xpolicy, XPOLICY);
	if(rc<0) {
		printf("Could not read execute policy tags.\n");
		return rc;
	}

	xname_size = sizeof(XPOLICY_XATTR) + sizeof(int);
	xname = (char*) malloc(xname_size);

	printf("%s: %d\n",XPOLICY_COUNT, xpolicy.size);
	for (i=0; i<xpolicy.size; i++) {
		snprintf (xname, xname_size,"%s%d", XPOLICY_XATTR,i);
		printf("%s: ",xname);
		for(j=0;j<xpolicy.tags[i].size;j++)
			printf("%d ",xpolicy.tags[i].values[j]);
		printf("\n");
	}

	free_policy(&xpolicy);
	free(xname);
	return rc;
}

