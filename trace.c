#include <stdio.h>
#include <sys/types.h>
#include <attr/xattr.h> // depends on package libattr1-dev

#define ATTR_NAME "security.blare_trace"

int main (int argc, char **argv){
    char *filename, *action;
    int attr, x = 0;

    // argc should at least be 2 
    if (argc < 2){
        printf("Usage: trace [binary] [on|off]\n");
        return -1;
    }

    filename = argv[1];

    /* Read */
    if (argc == 2){
        x = getxattr(filename, ATTR_NAME, &attr, sizeof(int));
        if (x < 0)
            printf("Error %d reading xattr\n", x);
        else
            printf("--> tracing: %d [off=0; on=1]\n", attr);

        return attr;
    }

    /* Write */
    action = argv[2];

    if (strncmp(action, "off", 2) == 0){
        attr = 0;
        printf("--> Tracing OFF\n");
    }

    else if (strncmp(action, "on", 2) == 0){
        attr = 1;
        printf("--> Tracing ON\n");
    }

    else{
        printf("Please provide on|off. Exiting..\n");
        return -1;
    }

    x = setxattr(filename, ATTR_NAME, &attr, sizeof(int), 0x0);

    if (x < 0)
        printf("Error %d setting extended attribute :(\n", x);

    return x;
}
