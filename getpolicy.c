/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include <sys/types.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <stdlib.h>
#include "lib.h"
#include <regex.h>

int main (int argc, char **argv){
	char *filename, *xname;
	int xname_size, rc, i, j;
	struct policy_t policy;

	if (argc < 2){
		printf("Usage: getpolicy filename\n");
		return -1;
	}

	filename = argv[1];
	rc = get_policy_from_file(filename, &policy, POLICY);
	if(rc<0) {
		printf("Could not read policy tags.\n");
		return rc;
	}

	xname_size = sizeof(POLICY_XATTR) + sizeof(int);
	xname = (char*) malloc(xname_size);

	printf("%s: %d\n",POLICY_COUNT, policy.size);
	for (i=0; i<policy.size; i++) {
		snprintf (xname, xname_size,"%s%d", POLICY_XATTR,i);
		printf("%s: ",xname);
		for(j=0;j<policy.tags[i].size;j++)
			printf("%d ",policy.tags[i].values[j]);
		printf("\n");
	}

	free_policy(&policy);
	free(xname);
	return rc;
}

