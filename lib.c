/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include "lib.h"
#include <sys/types.h>
#include <attr/xattr.h> // depends on package libattr1-dev
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <errno.h>
#include <string.h>





/* Used by qsort as a comparation function */
int compare (const void *a, const void *b){
	const int *ia = (const int*)a;
	const int *ib = (const int*)b;
	return (*ia > *ib) - (*ia < *ib);
}




/* Get the number extended attributes for file @filename.
 * Returns the number of attributes or the error code.
 * */
int get_list_length(char *filename){
	int rc;

	rc = listxattr(filename, NULL,0);

	return rc;
}

/* Get the list of extended attributes for file @filename.
 * Returns the number of attributes or the error code.
 * */
int get_list(char *filename, char *list, int n){
	int rc;

	rc = listxattr(filename,list,n);
	if (rc<0){
		return rc;
	}

	return rc;
}

/* Allocate and free the memory needed to store tags
 * */
void init_tags(struct tags_t *tags, int n) {
	tags->size = n;
	tags->values = (int*) malloc(n * sizeof(int));
}

void free_tags(struct tags_t *tags) {
	tags->size = 0;
	free(tags->values);
}

/* get_tags_* functions allocate the memory needed to store the extended
 * attributes, then fetch and store these attributes.
 * The memory should be freed with free_tags afterwards
 * */
int get_tags_from_file(char *filename, char *xattr_name, struct tags_t *tags) {
	int n, rc;

	n = getxattr(filename, xattr_name, NULL , 0)/sizeof(int);
	if (n<0)
		return n;

	init_tags(tags, n);

	rc = getxattr(filename, xattr_name,
			tags->values, tags->size * sizeof(int));
	return rc;
}

int get_tags_from_command(int argc, char *argv[], struct tags_t *tags) {
	int n, i;

	n = argc - 2;
	if (n<1)
		return -1;

	// The number of tags in argv[] is argc -2 (the two first 
	// values are the program name and the filename)
	init_tags(tags, n);

	for(i=0; i<n; i++)
		tags->values[i] = atoi(argv[i+2]);

	// Now let's sort it
	qsort(tags->values,n,sizeof(int),compare);

	// Return the number of tags
	return n;
}

int write_tags(char *filename, char *xattr_name, struct tags_t tags) {
	int rc;

	rc = setxattr(filename, xattr_name,
			tags.values, tags.size * sizeof(int),
			0x0);
	/* 0x0 = XATTR_CREATE|XATTR_REPLACE); */

	return rc;
}


/* Allocate and free the memory needed to store a policy
 * */
void init_policy(struct policy_t *policy, int n) {
	policy->size = n;
	policy->tags = (struct tags_t *) malloc(n * sizeof(struct tags_t));
}

void free_policy(struct policy_t *policy) {
	int i;
	for(i=0; i<policy->size; i++)
		free_tags(&policy->tags[i]);
	policy->size = 0;
	free(policy->tags);
}

/* get_policy_* functions allocate the memory needed to store the extended
 * attributes, then fetch and store these attributes.
 * The memory should be freed with free_tags afterwards
 * */
int get_policy_from_file(char *filename, struct policy_t *policy, type_t mode) {
	int xname_size, rc, i;
	char *PCOUNT, *PATTR, *xname;
	struct tags_t buffer;

	switch (mode) {
	case POLICY:
		xname_size = sizeof(POLICY_XATTR) + sizeof(int);
		PCOUNT = POLICY_COUNT;
		PATTR = POLICY_XATTR;
		break;
	case XPOLICY:
		xname_size = sizeof(XPOLICY_XATTR) + sizeof(int);
		PCOUNT = XPOLICY_COUNT;
		PATTR = XPOLICY_XATTR;
		break;
	case ROLE:
		xname_size = sizeof(ROLE_XATTR) + sizeof(int);
		PCOUNT = ROLE_COUNT;
		PATTR = ROLE_XATTR;
		break;
	}
	xname = (char*) malloc(xname_size);

	init_tags(&buffer, 1);
	rc = getxattr(filename, PCOUNT, buffer.values, sizeof(int));
	if (rc < 0)
		return rc;
	init_policy(policy, buffer.values[0]);
	free_tags(&buffer);

	for (i=0; i<policy->size; i++) {
		snprintf (xname, xname_size,"%s%d", PATTR,i);
		rc = get_tags_from_file(filename, xname, &policy->tags[i]);
		if (rc<0)
			return rc;
	}

	free(xname);
	return rc;
}

int get_policy_from_command(int argc, char *argv[], struct policy_t *policy) {
	regex_t open,close; 
	const char* delimiters = "{}";
	char *token, *tmp;
	int opening=0, closing=0, count=0, position=0, rc, i, j, tag;

	// Compile regexps to match the delimiters
	rc = regcomp(&open, "{", 0);
	rc = regcomp(&close, "}", 0);

	while (position < argc) {
		// Find the position of the opening delimiter
		for(i=position;i<argc;i++){
			if (regexec(&open, argv[opening], 0, NULL, 0))
				opening = i;
		}

		// Find the position of the closing delimiter
		for (i=opening;i<argc;i++){
			if (regexec(&close, argv[closing], 0, NULL, 0))
				closing = i;
		}

		// Some particular cases
		if (opening > closing)
			return -1;
		else if (opening == 0 || closing == 0)
			return -2;

		position = closing +1;
		opening = position;
		closing = position;
		count++;
	}
	init_policy(policy, count);

	opening = 0;
	closing = 0;
	position = 0;
	j=0;
	tmp=NULL;
	while (position < argc) {
		// Find the position of the opening delimiter
		for(i=position;i<argc;i++){
			if (regexec(&open, argv[opening], 0, NULL, 0))
				opening = i;
		}

		// Find the position of the closing delimiter
		for (i=opening;i<argc;i++){
			if (regexec(&close, argv[closing], 0, NULL, 0))
				closing = i;
		}

		// Some particular cases
		if (opening > closing)
			return -1;
		else if (opening == 0 || closing == 0)
			return -2;

		init_tags(&policy->tags[j], closing - opening +1);
		count = 0;
		for(i = opening; i <= closing; i++){
			tmp=(char*)realloc(tmp,sizeof(argv[i]));
			strcpy(tmp,argv[i]);
			token = strtok(tmp,delimiters);
			tag = strtoimax(token,NULL,10);
			if (tag != 0){
				policy->tags[j].values[count] = tag;
				count ++;
			}
		}

		position = closing +1;
		opening = position;
		closing = position;
		j++;
	}

	free(tmp);
	return 0;
}

int write_policy(char *filename, struct policy_t policy, type_t mode) {
	int xname_size, rc, i;
	char *PCOUNT, *PATTR, *xname;

	switch (mode) {
	case POLICY:
		xname_size = sizeof(POLICY_XATTR) + sizeof(int);
		PCOUNT = POLICY_COUNT;
		PATTR = POLICY_XATTR;
		break;
	case XPOLICY:
		xname_size = sizeof(XPOLICY_XATTR) + sizeof(int);
		PCOUNT = XPOLICY_COUNT;
		PATTR = XPOLICY_XATTR;
		break;
	case ROLE:
		xname_size = sizeof(ROLE_XATTR) + sizeof(int);
		PCOUNT = ROLE_COUNT;
		PATTR = ROLE_XATTR;
		break;
	}
	xname = (char*) malloc(xname_size);

	for (i=0; i<policy.size; i++) {
		snprintf (xname, xname_size,"%s%d", PATTR, i);
		rc = write_tags(filename, xname, policy.tags[i]);
		if (rc<0)
			return rc;
	}


	rc = setxattr(filename, PCOUNT, &policy.size, sizeof(int), 0x0);

	free(xname);
	return rc;
}

