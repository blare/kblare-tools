/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */


#include <stdbool.h>


#define DEBUG false


#define INFO_XATTR "security.blare.info"
#define POLICY_XATTR "security.blare.policy"
#define POLICY_COUNT "security.blare.policy_count"
#define POLICY_VERSION "security.blare.policy_version"

#define XPOLICY_XATTR "security.blare.xpolicy"
#define XPOLICY_COUNT "security.blare.xpolicy_count"
#define XPOLICY_VERSION "security.blare.xpolicy_version"

#define ROLE_XATTR "security.blare.role"
#define ROLE_COUNT "security.blare.role_count"

typedef enum {
	POLICY,
	XPOLICY,
	ROLE
} type_t;

struct tags_t {
	int size;
	int *values;
};

struct policy_t {
	int size;
	struct tags_t *tags;
};

int get_list_length(char *filename);
int get_list(char *filename, char *list, int n);

int get_tags_from_file(char *filename, char *xattr_name, struct tags_t *tags);
int get_tags_from_command(int argc, char *argv[], struct tags_t *tags);
int write_tags(char *filename, char *xattr_name, struct tags_t tags);
void free_tags(struct tags_t *tags);

int get_policy_from_file(char *filename, struct policy_t *policy, type_t mode);
int get_policy_from_command(int argc, char *argv[], struct policy_t *policy);
int write_policy(char *filename, struct policy_t policy, type_t mode);
void free_policy(struct policy_t *policy);

