/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include <sys/types.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <stdlib.h>
#include "lib.h"
#include <regex.h>


int main (int argc, char **argv){
	char *uid, *filename;
	const char *folder = "/etc/blare/";
	int pathsize, rc;
	struct policy_t role;

	if (argc < 2){
		printf("Usage: setpolicy role_number/uid {tag1 ...} [{...]\n");
		return -1;
	}

	uid = argv[1];
	pathsize = sizeof(*uid) + sizeof(*folder);
	filename = (char*)malloc(pathsize);

	snprintf(filename,pathsize,"%s%s",folder,uid);

	rc = get_policy_from_command(argc, argv, &role);
	if (rc <0) {
		printf("Could not parse role properly.\n");
		return rc;
	}

	rc = write_policy(filename, role, ROLE);
	if (rc <0) {
		printf("Could not write role.\n");
		return rc;
	}

	free_policy(&role);
	free(filename);

	printf("Successfuly wrote role\n");
	return rc;

}


