/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include "lib.h"
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
	int rc;
	char *filename;

	if (argc < 2) {
		printf("Usage: delinfo filename\n");
		return -1;
	}

	filename = argv[1];

	rc = removexattr(filename, INFO_XATTR);
	if (rc < 0){
		printf("Error code %d\n",rc);
		return rc;
	}
	printf("Removed information tags.\n");
	return 0;
}
